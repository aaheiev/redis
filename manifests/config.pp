class redis::config (
  $ip             = '127.0.0.1',
  $port           = 6379,
  $unixsocket     = '/var/run/redis/redis.sock',
  $unixsocketperm = 777,
  $loglevel       = notice,
  $maxmemory      = '4Gb',
  $password       = false,
  $ensure         = present,) {
  include redis::install
  include redis::service

  file { "redis.conf":
    path    => "/etc/redis/conf.d/local.conf",
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => 0644,
    content => template("redis/redis.local.conf.erb"),
    require => Class["redis::install"],
    notify  => Class["redis::service"],
  }
}
