class redis::install ($ensure = 'installed', $package = 'redis-server',) {
  include redis::repo

  package { $package:
    ensure  => $ensure,
    require => Class["redis::repo"],
  }
}
