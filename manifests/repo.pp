class redis::repo ($ensure = 'installed') {
  include apt

  apt::ppa { "ppa:rwky/redis": }
}
